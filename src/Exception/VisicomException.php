<?php

declare(strict_types=1);

namespace Visicom\SDK\Exception;

use Throwable;

interface VisicomException extends Throwable
{

}