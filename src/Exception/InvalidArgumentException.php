<?php

declare(strict_types=1);

namespace Visicom\SDK\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements VisicomException
{

}