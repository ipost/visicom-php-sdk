<?php

declare(strict_types=1);

namespace Visicom\SDK\Exception;

use Exception;

class NotFoundException extends Exception implements VisicomException
{

}