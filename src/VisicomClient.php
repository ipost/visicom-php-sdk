<?php

declare(strict_types=1);

namespace Visicom\SDK;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Visicom\SDK\Entity\Address;
use Visicom\SDK\Exception\ClientException;
use Visicom\SDK\Exception\HttpResponseException;
use Visicom\SDK\Exception\NotFoundException;
use Visicom\SDK\Request\GeocodeRequest;
use Visicom\SDK\Utils\Hydrator;

class VisicomClient
{
    private ClientInterface $client;
    private LoggerInterface $logger;
    private Hydrator $hydrator;
    private string $accessToken;

    public function __construct(
        string $accessToken,
        ClientInterface  $client,
        ?LoggerInterface $logger = null
    ) {
        $this->client = $client;
        $this->accessToken = $accessToken;
        $this->logger = $logger ?? new NullLogger();
        $this->hydrator = new Hydrator();
    }

    /**
     * @throws HttpResponseException
     * @throws ClientException
     */
    private function send(string $url): \stdClass
    {
        $request = new Request('GET', $url);

        $this->logger->debug('Visicom.Url', ['url' => $url]);

        try {
            $response = $this->client->sendRequest($request);
        } catch (ClientExceptionInterface $e) {
            $this->logger->error('Visicom.Client', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile() . ':' . $e->getLine(),
            ]);
            throw new ClientException();
        }

        $json = $response->getBody()->__toString();

        if ($response->getStatusCode() === 200) {
            $this->logger->debug('Visicom.Response', ['content' => $json]);
            return json_decode($json);
        } else if ($response->getStatusCode() === 400) {
            $result = json_decode($json);
            if (isset($result->http_status)) {
                $this->logger->error('Visicom.Response', [
                    'code' => $response->getStatusCode(),
                    'message' => $result->http_status . '. ' . $result->message,
                ]);
            }
        } else {
            $this->logger->error('Visicom.Http', [
                'code' => $response->getStatusCode(),
                'message' => $response->getReasonPhrase(),
            ]);
        }

        throw new HttpResponseException($response->getReasonPhrase(), $response->getStatusCode());
    }

    /**
     * @throws NotFoundException
     * @throws HttpResponseException
     * @throws ClientException
     */
    public function findAddress(string $text, string $lang = 'ru'): Address
    {
        $request = (new GeocodeRequest())
            ->withCategories('adr_address')
            ->withCountry('ua')
            ->withText($text)
            ->withLimit(1)
            ->withLang($lang)
            ->withKey($this->accessToken);

        $result = $this->send($request->build());

        if (empty($result->type)) {
            throw new NotFoundException();
        }

        $address = $this->hydrator->fromArray(Address::class, [
            'country' => $result->properties->country,
            'settlement' => $result->properties->settlement,
            'settlement_type' => $result->properties->settlement_type,
            'street' => $result->properties->street,
            'street_type' => $result->properties->street_type,
            'number' => $result->properties->name,
            'lat' => $result->geo_centroid->coordinates[1],
            'lng' => $result->geo_centroid->coordinates[0],
        ]);

        return $address;
    }

    /**
     * @throws NotFoundException
     * @throws HttpResponseException
     * @throws ClientException
     *
     * @return Address[]
     */
    public function findAddresses(string $text, string $lang = 'ru'): array
    {
        $request = (new GeocodeRequest())
            ->withCategories('adr_address')
            ->withCountry('ua')
            ->withText($text)
            ->withLang($lang)
            ->withKey($this->accessToken);

        $result = $this->send($request->build());

        $features = [];
        $addresses = [];

        if (empty($result->type)) {
            throw new NotFoundException();
        } else if ($result->type === 'FeatureCollection') {
            $features = $result->features;
        } else if ($result->type === 'Feature') {
            $features = $result;
        }

        foreach ($features as $item) {
            $addresses[] = $this->hydrator->fromArray(Address::class, [
                'country' => $item->properties->country,
                'settlement' => $item->properties->settlement,
                'settlement_type' => $item->properties->settlement_type,
                'street' => $item->properties->street,
                'street_type' => $item->properties->street_type,
                'number' => $item->properties->name,
                'lat' => $item->geo_centroid->coordinates[1],
                'lng' => $item->geo_centroid->coordinates[0],
            ]);
        }

        return $addresses;
    }

    /**
     * @throws NotFoundException
     * @throws HttpResponseException
     * @throws ClientException
     *
     * @return Address[]
     */
    public function find(string $text, string $lang = 'ru'): array
    {
        $request = (new GeocodeRequest())
            ->withCategories('adm_settlement,adr_address,adr_street')
            ->withCountry('ua')
            ->withText($text)
            ->withLang($lang)
            ->withKey($this->accessToken);

        $result = $this->send($request->build());

        $features = [];
        $addresses = [];

        if (empty($result->type)) {
            throw new NotFoundException();
        } else if ($result->type === 'FeatureCollection') {
            $features = $result->features;
        } else if ($result->type === 'Feature') {
            $features = $result;
        }

        foreach ($features as $item) {
            $addresses[] = $this->hydrator->fromArray(Address::class, [
                'country' => $item->properties->country,
                'settlement' => $item->properties->settlement,
                'settlement_type' => $item->properties->settlement_type,
                'street' => $item->properties->street,
                'street_type' => $item->properties->street_type,
                'number' => $item->properties->name,
                'lat' => $item->geo_centroid->coordinates[1],
                'lng' => $item->geo_centroid->coordinates[0],
            ]);
        }

        return $addresses;
    }

    /**
     * @throws NotFoundException
     */
    public function near(float $lat, float $lng, int $radius, string $lang = 'ru'): Address
    {
        $request = (new GeocodeRequest())
            ->withCategories('adr_address')
            ->withCountry('ua')
            ->withNear($lng . ',' . $lat)
            ->withRadius($radius)
            ->withLimit(1)
            ->withLang($lang)
            ->withKey($this->accessToken);

        $result = $this->send($request->build());

        if (empty($result->type)) {
            throw new NotFoundException();
        }

        $address = $this->hydrator->fromArray(Address::class, [
            'country' => $result->properties->country,
            'settlement' => $result->properties->settlement,
            'settlement_type' => $result->properties->settlement_type,
            'street' => $result->properties->street,
            'street_type' => $result->properties->street_type,
            'number' => $result->properties->name,
            'lat' => $result->geo_centroid->coordinates[1],
            'lng' => $result->geo_centroid->coordinates[0],
        ]);

        return $address;
    }
}