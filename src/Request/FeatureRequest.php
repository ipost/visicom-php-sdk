<?php

declare(strict_types=1);

namespace Visicom\SDK\Request;

class FeatureRequest
{
    private const DEFAULT_URL = 'https://api.visicom.ua/data-api/5.0/[lang]/feature/[id].json';

    private ?string $key = null;            // Ключ авторизации.
    private string $id;                     // Идентификатор объекта. В параметре можно передать несколько идентификаторов объектов, разделенных запятой. Максимальное количество объектов - 250.
    private string $lang = 'ru';            // Язык ответа. Один из (uk, en, ru).
    private ?string $geometry = null;       // Параметр указывает, возвращать ли в ответе геометрию объекта. Может принимать значение: no — не возвращать геометрию (поле geometry отсутсвует). Если параметр не указан, то геометрия в ответе включается.
    private ?string $callback = null;       // Имя функции для JSONP запроса к серверу.
    private string $request_schema =  self::DEFAULT_URL; // Схема запроса

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function build(): string
    {
        $url = $this->request_schema;

        $params = [
            'lang' => $this->lang,
            'id' => $this->id,
        ];

        foreach ($params as $name => $value) {
            $url = str_replace('[' . $name . ']', $value, $url);
        }

        return $url . '?' . http_build_query([
            'key' => $this->key,
            'geometry' => $this->geometry,
            'callback' => $this->callback,
        ]);
    }

    public function withKey(string $key): self
    {
        $new = clone $this;
        $new->key = $key;
        return $new;
    }

    public function withLang(string $lang): self
    {
        $new = clone $this;
        $new->lang = $lang;
        return $new;
    }

    public function withGeometry(string $geometry): self
    {
        $new = clone $this;
        $new->geometry = $geometry;
        return $new;
    }

    public function withCallback(string $callback): self
    {
        $new = clone $this;
        $new->callback = $callback;
        return $new;
    }

    public function withRequestSchema(string $request_schema): self
    {
        $new = clone $this;
        $new->request_schema = $request_schema;
        return $new;
    }
}