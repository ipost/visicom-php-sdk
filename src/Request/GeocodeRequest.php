<?php

declare(strict_types=1);

namespace Visicom\SDK\Request;

class GeocodeRequest
{
    private const DEFAULT_URL = 'https://api.visicom.ua/data-api/5.0/[lang]/geocode.json';

    private ?string $key = null;                // Ключ авторизации.
    private string $lang = 'ru';                // Язык ответа. Один из (uk, en, ru).
    private ?string $categories = null;         // Список идентификаторов категорий через «,» в которых будет выполняться поиск объектов.
    private ?string $categories_exclude = null; // Список идентификаторов категорий через «,» которые будут исключены из результатов поиска.
    private ?string $text = null;               // Текст, который должен встречаться в описании объекта. Пример: text=Крещатик, t=Жовтень.
    private ?string $word_text = null;          // Слова, которые должны встречаться в описании объекта. Пример: word_text=Киев. В поиск не попадут Киевский, Киевская и т.д.
    private ?string $near = null;               // Идентификатор объекта или геометрия в формате WKT с количеством вершин не более 250. Для точек возможна упрощенная запись в виде lng,lat. Расстояние до указанной геометрии будет учитываться при формировании результата. Пример: near=POIA1KIGKN, n=30.5113,50.4550.
    private ?int $radius = null;             // Радиус в метрах вокруг местоположения, которое задано параметром near.
    private ?string $order = null;              // Параметр указывает тип сортировки объектов в ответе. Может принимать значения relevance (сортировка по релевантности объектов для данного запроса), distance (сортировка по удаленности от точки, указанной параметром near). По умолчанию - значение relevance.
    private ?string $intersect = null;          // Идентификатор объекта или геометрия в формате WKT с количеством вершин не более 250. Для точек возможна упрощенная запись в виде lng,lat. Запрос возвращает объекты, геометрия которых пересекается с геометрией, описанной данным параметром.
    private ?string $contains = null;           // Идентификатор объекта или геометрия в формате WKT с количеством вершин не более 250. Для точек возможна упрощенная запись в виде lng,lat. Запрос возвращает объекты, геометрия которых находится внутри геометрии, описанной данным параметром.
    private ?string $zoom = null;               // Масштаб карты по спецификации TMS. Чем меньше масштаб, тем меньше учитываются координаты, указанные параметром near при расчете релевантности объектов.
    private ?int $limit = null;              // Максимальное количество возвращаемых объектов. Максимум 250.
    private ?string $country = null;            // Код страны.
    private ?string $boost_country = null;      // Код страны. Если не указан параметр country, то объекты в указанной этим параметром стране имеют больший приоритет.
    private ?string $callback = null;           // Имя функции для JSONP запроса к серверу.
    private string $request_schema = self::DEFAULT_URL;    // Схема запроса
    
    public function build(): string
    {
        $url = $this->request_schema;

        $params = [
            'lang' => $this->lang,
        ];

        foreach ($params as $name => $value) {
            $url = str_replace('[' . $name . ']', $value, $url);
        }

        return $url . '?' . http_build_query([
                'key' => $this->key,
                'categories' => $this->categories,
                'categories_exclude' => $this->categories_exclude,
                'text' => $this->text,
                'word_text' => $this->word_text,
                'near' => $this->near,
                'radius' => $this->radius,
                'order' => $this->order,
                'intersect' => $this->intersect,
                'contains' => $this->contains,
                'zoom' => $this->zoom,
                'limit' => $this->limit,
                'country' => $this->country,
                'boost_country' => $this->boost_country,
                'callback' => $this->callback,
            ]);
    }

    public function withKey(string $key): self
    {
        $new = clone $this;
        $new->key = $key;
        return $new;
    }

    public function withLang(string $lang): self
    {
        $new = clone $this;
        $new->lang = $lang;
        return $new;
    }

    public function withCategories(string $categories): self
    {
        $new = clone $this;
        $new->categories = $categories;
        return $new;
    }

    public function withCategoriesExclude(string $categories_exclude): self
    {
        $new = clone $this;
        $new->categories_exclude = $categories_exclude;
        return $new;
    }

    public function withText(string $text): self
    {
        $new = clone $this;
        $new->text = $text;
        return $new;
    }

    public function withWordText(string $word_text): self
    {
        $new = clone $this;
        $new->word_text = $word_text;
        return $new;
    }

    public function withNear(string $near): self
    {
        $new = clone $this;
        $new->near = $near;
        return $new;
    }

    public function withRadius(int $radius): self
    {
        $new = clone $this;
        $new->radius = $radius;
        return $new;
    }

    public function withOrder(string $order): self
    {
        $new = clone $this;
        $new->order = $order;
        return $new;
    }

    public function withIntersect(string $intersect): self
    {
        $new = clone $this;
        $new->intersect = $intersect;
        return $new;
    }

    public function withContains(string $contains): self
    {
        $new = clone $this;
        $new->contains = $contains;
        return $new;
    }

    public function withZoom(string $zoom): self
    {
        $new = clone $this;
        $new->zoom = $zoom;
        return $new;
    }

    public function withLimit(int $limit): self
    {
        $new = clone $this;
        $new->limit = $limit;
        return $new;
    }

    public function withCountry(string $country): self
    {
        $new = clone $this;
        $new->country = $country;
        return $new;
    }

    public function withBoostCountry(string $boost_country): self
    {
        $new = clone $this;
        $new->boost_country = $boost_country;
        return $new;
    }

    public function withCallback(string $callback): self
    {
        $new = clone $this;
        $new->callback = $callback;
        return $new;
    }

    public function withRequestSchema(string $request_schema): self
    {
        $new = clone $this;
        $new->request_schema = $request_schema;
        return $new;
    }

    public function isKey(): bool
    {
        return (bool) $this->key;
    }
}