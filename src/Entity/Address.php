<?php

declare(strict_types=1);

namespace Visicom\SDK\Entity;

class Address
{
    public string $country;
    public string $settlement_type;
    public string $settlement;
    public string $street_type;
    public string $street;
    public string $number;
    public float $lat;
    public float $lng;
}